const { render } = require("ejs")
const express = require("express")
const app = express()
const admin = require("./api/admin")

app.set("view engine", "ejs")

app.use("/api/v1", admin)

// List view
app.get("/", (req, res) => {
    res.render("admin/list")
})

// form create
app.get("/admin/form", (req, res) => {
    res.render("admin/form", { id: null })
})

// form edit
app.get("/admin/form/:id", (req, res) => {
    res.render("admin/form", { id: req.params.id })
})

// Detail view
app.get("/admin/:id", (req, res) => {
    res.render("admin/detail", {
        id: req.params.id
    })
})

app.listen(8000, () => console.log(`Server running on port 8000`))