'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Account, {
        foreignKey: { name: "account_id", type: DataTypes.UUID },
        as: "account",
      });
      User.hasMany(models.History, {
        foreignKey: { name: "id", type: DataTypes.INTEGER },
        as: "history",
      });
      models.Account.belongsTo(User, {
        foreignKey: { name: "id", type: DataTypes.INTEGER },
      });
      models.History.belongsTo(User, {
        foreignKey: { name: "id", type: DataTypes.INTEGER },
      });
    }
  }
  User.init({
    user_id: DataTypes.UUID,
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};