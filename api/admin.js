const express = require("express")
const router = express.Router()
const cors = require("cors")
const { Account, History, User } = require("../models")

router.use(cors())
router.use(express.json())

// List all articles
router.get("/admin", async (req, res) => {
    let response = []
    try {
        response = await User.findAll({
            include: [
                {
                    model: Account,
                    as: "account"
                },
                {
                    model: History,
                    as: "history"
                }
            ]
        })
    } catch (e) {
        console.log(e.message)
    }

    res.json(response)
})

// Get single article by id
router.get("/admin/:id", async (req, res) => {
    let response = {}
    try {
        response = await User.findOne({where: {id: req.params.id},
            include: [
                {
                    model: Account,
                    as: "account"
                },
                {
                    model: History,
                    as: "history"
                }
            ]
        })
    } catch (e) {
        console.log(e.message)
        response.error = e.message
        res.status(401)
    }

    res.json(response)
})

// Create new article
router.post("/admin", async (req, res) => {
    try { let User = await User.create({
            user_id: req.body.user_id,
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            createdAt: new Date(),
            updatedAt: new Date(),
        })

        let Account = await Account.create({
            account_id: req.body.account_id,
            username: req.body.username,
            rank: req.body.rank,
            createdAt: new Date(),
            updatedAt: new Date(),
        })

        let History = await History.create({
            history_id: req.body.history_id,
            match_id: req.body.match_id,
            match_count: req.body.match_count,
            win: req.body.win,
            lose: req.body.lose,
            createdAt: new Date(),
            updatedAt: new Date(),
        })
    } catch (e) {
        console.log(e)
        console.log(e.message)
    }

    res.end()
})

// Update article by id
router.put("/admin/:id", async (req, res) => {
    try { let User = await User.update({
            user_id: req.body.user_id,
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            updatedAt: new Date(),
        }, {
            where: {id: req.params.id}
        })

          let Account = await Account.update({
            account_id: req.body.account_id,
            username: req.body.username,
            rank: req.body.rank,
            updatedAt: new Date(),
        }, {
            where: {account_id: req.params.id}
        })

          let History = await History.update({
            history_id: req.body.history_id,
            match_id: req.body.match_id,
            match_count: req.body.match_count,
            win: req.body.win,
            lose: req.body.lose,
            updatedAt: new Date(),
        }, {
            where: {id: req.params.id}
        })
        
    } catch (e) {
        console.log(e.message)
    }

    res.end()
})

// Remove article by id
router.delete("/admin/:id", async (req, res) => {
    try {
        await User.destroy({
            where: {id: req.params.id}.then(function(){
                console.log('Destroy all "id" rows');
                res.redirect('/');
            })
        })
    } catch (e) {
        console.log(e.message)
    }

    res.end()
})

module.exports = router