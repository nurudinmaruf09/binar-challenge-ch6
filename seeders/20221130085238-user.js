'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Users', [{
      id: 1,
      user_id: 1001,
      name: "udin",
      email: "nurudinmaruf@gmail.com",
      password: "123456",
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      id: 2,
      user_id: 1002,
      name: "sabrina",
      email: "binaracademy@gmail.com",
      password: "123456",
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
