'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Accounts', [{
      id: 1,
      account_id: 2101,
      username: "udin",
      rank: 7,
      createdAt: new Date (),
      updatedAt: new Date (),
    }, {
      id: 2,
      account_id: 2102,
      username: null,
      rank: null,
      createdAt: new Date (),
      updatedAt: new Date (),
    }, {
      id: 3,
      account_id: 2103,
      username: null,
      rank: null,
      createdAt: new Date (),
      updatedAt: new Date (),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
