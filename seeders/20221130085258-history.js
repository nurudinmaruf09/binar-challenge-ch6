'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     await queryInterface.bulkInsert('Histories', [{
      id: 1,
      history_id: 1,
      match_id: 1001,
      last_played: new Date(),
      match_count: 20,
      win: 13,
      lose: 7, 
      createdAt: new Date(),
      updatedAt: new Date(),
     }, {
      id: 2,
      history_id: 2,
      match_id: 1002,
      last_played: null,
      match_count: null,
      win: null,
      lose: null, 
      createdAt: new Date(),
      updatedAt: new Date(),
     }, {
      id: 3,
      history_id: 3,
      match_id: 1003,
      last_played: null,
      match_count: null,
      win: null,
      lose: null, 
      createdAt: new Date(),
      updatedAt: new Date(),
     }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
